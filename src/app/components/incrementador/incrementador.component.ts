import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [
  ]
})
export class IncrementadorComponent implements OnInit {

  /**
   * Para renombrar la variable se pone el nuevo nombre en Input y al ser utilizado: 
   * <app-incrementador [valor]="80"></app-incrementador>  
   * @Input('valor') progreso: number;
   */
  @Input() progreso: number;
  @Input() btnClass: string;

  @Output('progreso') progresoSalida: EventEmitter<number> = new EventEmitter();

  constructor() {
    this.progreso = 50;
    this.btnClass = 'btn btn-primary';
  }

  ngOnInit(): void {
  }

  get getPorcentaje() {
    return `${this.progreso}%`;
  }

  cambiarValor(valor: number) {
    if(this.progreso >=100 && valor >= 0){
      this.progresoSalida.emit(100);
      return this.progreso = 100;
    }
    if(this.progreso <=0 && valor < 0){
      this.progresoSalida.emit(0);
      return this.progreso = 0;
    }
    this.progreso = this.progreso + valor;
    this.progresoSalida.emit(this.progreso);
  }


  onChange(valor: number){
    if(valor >= 100){
      this.progreso = 100;
    } else if(valor <= 0 ){
      this.progreso = 0;
    } else {
      this.progreso = valor;
    }
    
    this.progresoSalida.emit(valor);
  }

}
